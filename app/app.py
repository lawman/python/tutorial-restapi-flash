from flask import Flask
from . import controller
from .controller.store_controller import blueprint

app = Flask(__name__)


# def register_blueprints(app):
#     """Register Flask blueprints."""
#
#     return None
app.register_blueprint(blueprint)

if __name__ == '__main__':

    app.run()
