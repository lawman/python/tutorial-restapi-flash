class StoreRepository:
    def __init__(self):
        self.stores = [
            {
                "name": "My Store",
                "items": [
                    {
                        "name": "Chair",
                        "price": 15.99
                    }
                ]
            }
        ]

    def get_stores(self):
        return self.stores

    def create_store(self, name):
        new_store = {"name": name, "items": []}
        self.stores.append(new_store)
        return new_store, 201

    def create_item(self, store_name, item_name, item_price):
        for store in self.stores:
            if store["name"] == store_name:
                new_item = {"name": item_name, "price": item_price}
                store["items"].append(new_item)
                return new_item, 201
        return {"message": "Store not found"}, 404

    def get_store(self, name):
        for store in self.stores:
            if store["name"] == name:
                return store
        return {"message": "Store not found"}, 404

    def get_items_in_store(self, name):
        for store in self.stores:
            if store["name"] == name:
                return {"items": store["items"]}
        return {"message": "Store not found"}, 404
