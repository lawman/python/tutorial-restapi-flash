from app.repository.store_repository import StoreRepository


class StoreService:
    def __init__(self):
        self.store_repository = StoreRepository()

    def get_stores(self):
        return self.store_repository.get_stores()

    def create_store(self, name):
        return self.store_repository.create_store(name)

    def create_item(self, store_name, item_name, item_price):
        return self.store_repository.create_item(store_name, item_name, item_price)

    def get_store(self, name):
        return self.store_repository.get_store(name)

    def get_items_in_store(self, name):
        return self.store_repository.get_items_in_store(name)
