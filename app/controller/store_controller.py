from flask import request, Blueprint

from app.service.store_service import StoreService

blueprint = Blueprint('store_controller', __name__, url_prefix='/api/v1/stores')

store_service = StoreService()


@blueprint.get("")
def get_stores():
    return {"stores": store_service.get_stores()}


@blueprint.post("")
def create_store():
    request_data = request.get_json()
    return store_service.create_store(request_data["name"])


@blueprint.post("/<string:name>/item")
def create_item(name):
    request_data = request.get_json()
    return store_service.create_item(name, request_data["name"], request_data["price"])

#
# @app.get("/store/<string:name>")
# def get_store(name):
#     return store_service.get_store(name)
#
#
# @app.get("/store/<string:name>/item")
# def get_item_in_store(name):
#     return store_service.get_items_in_store(name)


# if __name__ == "__main__":
#     app.run(debug=True)
